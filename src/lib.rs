#![forbid(unsafe_code)]
#![warn(
    clippy::all,
    clippy::cargo,
    clippy::complexity,
    clippy::correctness,
    clippy::nursery,
    clippy::perf,
    clippy::pedantic,
    clippy::style,
    clippy::suspicious
)]
//#[feature(custom_inner_attributes)]
//#![clippy::msrv = "1.60.0"]

mod primitives;
pub use primitives::{Edge, Isomorphism, Node, Production};

mod graph;
pub use graph::Graph;

mod graphgrammar;
pub use graphgrammar::GraphGrammar;
pub use graphgrammar::QuitCondition;

mod graphoperations;
pub use graphoperations::{are_graphs_isomorph, match_subgraph};

pub mod predefined;
