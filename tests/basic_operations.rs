type Graph = graphlang::Graph<u32, String, ()>;
type GraphGrammar = graphlang::GraphGrammar<u32, String, ()>;

#[test]
fn read_graph_from_file() {
    let file = std::fs::File::open("tests/minimal.graph.json").unwrap();
    let reader = std::io::BufReader::new(file);
    let _g: Graph = serde_json::from_reader(reader).unwrap();
}

#[test]
fn serde_of_a_graphgrammar() {
    let gg: GraphGrammar = graphlang::predefined::ladder_grammar(3).into();
    let gg_ser = serde_json::to_string(&gg);
    assert!(gg_ser.is_ok());

    let gg_from_file: GraphGrammar = {
        let content = std::fs::read_to_string("tests/ladder_3.gg.json")
            .expect("ladder_3.gg.json is readable");
        serde_json::from_str(&content).expect("Deserialize json")
    };
    assert_eq!(gg, gg_from_file);
}

#[test]
fn graphgrammar_from_file_is_usable() {
    let gg: GraphGrammar = {
        let content = std::fs::read_to_string("tests/ladder_3.gg.json")
            .expect("ladder_3.gg.json is readable");
        serde_json::from_str(&content).expect("Deserialize json")
    };

    let mut graph = gg.start_graph.clone();
    for _ in 0..10 {
        gg.productions["extend"].apply_inplace(&mut graph);
    }
    gg.productions["finalize"].apply_inplace(&mut graph);

    assert!(gg.is_valid(&graph));
}
