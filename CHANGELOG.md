# CHANGELOG

## 0.1.2
- Fix typos in README.md 
- Add a basic application(-structure)
- Add dot file export functionality

## 0.1.1
- Add disclaimer about the state of the project.
- Add basic tests/

